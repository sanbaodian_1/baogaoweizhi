const QQMapWX = require('./qqmap-wx-jssdk.min.js');
const qqmapsdk = new QQMapWX({
  key: '5DLBZ-LI4KX-FPJ4F-7ZPFC-OVSUZ-FABJK'
});

const MapUtil = {
  getLocation:  function(callback) {
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success: function(res) {
        // 调用接口, 坐标转具体位置 -xxz0717
        qqmapsdk.reverseGeocoder({
          location: {
            latitude: Number(res.latitude),
            longitude: Number(res.longitude)
          },
          success: function(res) {
            console.log(res);
            callback(res);
          },
        });
      }
    })
  },
  getSuggestion: (keyword, callback) => {
    qqmapsdk.getSuggestion({
      keyword,
      //region:'北京', //设置城市名，限制关键词所示的地域范围，非必填参数
      success: function(res) { //搜索成功后的回调                
        console.log(res);
        callback(res);
      }
    });
  },
  reverseGeocoder: (param, callback) => {
    qqmapsdk.reverseGeocoder({
      location: param,
      success: (res) => {
        console.log(res);
        callback(res);
      }
    });
  }
}

module.exports = MapUtil;