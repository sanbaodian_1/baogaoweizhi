const app = getApp();
const MapUtil = require("./../../utils/map.js");

const db = wx.cloud.database();
const report = db.collection("Report");


Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: 0,
    types: ['通知'],
    tmpImgList: [],
    imgList: [],
    title: "",
    location: null,
    address: '',
    remark: "",
    polyline: null,
    markers: null,
    suggestList: []
  },
  PickerChange(e) {
    this.setData({
      index: parseInt(e.detail.value, 0)
    })
  },
  bindRemarkInput(e) {
    this.setData({
      remark: e.detail.value
    })
  },
  bindTitleInput(e) {
    this.setData({
      title: e.detail.value
    })
  },

  doUpload: function () {
    var that = this;

    let promiseArr = [];
    for (var t = 0; t < that.data.tmpImgList.length; t++) {
      promiseArr.push(new Promise((reslove, reject) => {
        const filePath = that.data.tmpImgList[t]

        // 上传图片

        const cloudPath = `${Date.now()}-${Math.floor(Math.random(0, 1) * 10000000)}.png`
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: res => {
            console.log('[上传文件] 成功：', res)
            that.setData({
              imgList: that.data.imgList.concat(res.fileID)
            });
            reslove();


          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })
      }));

    }
    Promise.all(promiseArr).then(res => {//等数组都做完后做then方法
      console.log("图片上传完成后再执行");
      const _ = db.command;
      db.collection('Member').doc(app.globalData.memberid).update({
        // data 字段表示需新增的 JSON 数据
        data: {
          jubao: _.inc(1),
        },
        success: function (res) {
          console.log('举报计次成功！');
        }
      })

      var {
        title,
        address,
        location,
        remark,
        imgList,
        index
      } = that.data;
      var item = {
        title,
        location: new db.Geo.Point(Number(that.data.location.longitude), Number(that.data.location.latitude)),
        address,
        remark,
        imgList,
        type: that.data.types[index],
        date: new Date()
      };
      report.add({
        data: item
      }).then(res => {
        wx.showToast({
          title: '通知发布成功',
          complete: () => {
            wx.redirectTo({
              url: '/pages/nearby/nearby',
            })
          }
        })
      }).catch(console.error)
    })
  },
  ChooseImage() {
    wx.chooseImage({
      count: 4, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.tmpImgList.length != 0) {
          this.setData({
            tmpImgList: this.data.tmpImgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            tmpImgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
  mapTap(e) {
  },
  inputChanged(e) {
    const keyword = e.detail.value;
    MapUtil.getSuggestion(keyword, res => {
      this.setData({
        suggestList: res.data
      });
    })
  },
  selectSuggest(e) {
    const id = e.currentTarget.id;
    for (var i = 0; i < this.data.suggestList.length; i++) {
      if (i == id) {
        this.setData({
          location: this.data.suggestList[i].location,
          address: this.data.suggestList[i].title,
          markers: [{
            iconPath: '/images/marker.png',
            id: 0,
            latitude: this.data.suggestList[i].location.lat,
            longitude: this.data.suggestList[i].location.lng,
            width: 32,
            height: 32
          }],
        });
        break;
      }
    }
    this.setData({
      suggestList: []
    })
  },
  loadMap() {
    MapUtil.getLocation((res) => {
      this.setData({
        location: res.result.location,
        address: res.result.address,
        polyline: [{
          points: [{
            latitude: res.result.location.lat,
            longitude: res.result.location.lng
          }, {
            latitude: res.result.location.lat,
            longitude: res.result.location.lng
          }],
          color: "#ff6600",
          width: 4,
          dottedLine: false,
          arrowLine: true,
          borderColor: "#000",
          borderWidth: 5
        }],
        markers: [{
          iconPath: '/images/marker.png',
          id: 0,
          latitude: res.result.location.lat,
          longitude: res.result.location.lng,
          width: 32,
          height: 32,
        }],
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.loadMap();
  },
  pageBack() {
    wx.navigateBack({
      delta: 1
    });
  },
  regionchange(e) {
    // 地图发生变化的时候，获取中间点，也就是用户选择的位置toFixed
    if (e.type == 'end') {
      console.log(e)
      const mapCtx = wx.createMapContext("map");
      mapCtx.getCenterLocation({
        type: 'gcj02',
        success: (res) => {
          console.log(res);
          mapCtx.translateMarker({
            markerId: 0,
            destination: {
              longitude: res.longitude,
              latitude: res.latitude
            },
            duration: 200,
            success: () => {
              const req = res;
              MapUtil.reverseGeocoder(req, (res) => {
                this.setData({
                  address: res.result.address,
                  location: {
                    latitude: req.latitude,
                    longitude: req.longitude
                  }
                });
              });
            }
          })
        },
        failed: (err) => {
          console.log(err)
        }
      })
    }
  },
  markertap(e) {
    console.log(e);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})