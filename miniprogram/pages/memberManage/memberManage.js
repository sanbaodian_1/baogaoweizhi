// pages/memberManage/memberManage.js
const app = getApp();
const MapUtil = require("./../../utils/map.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index_c: 0,
    index_c_original: 0,
    index_w: 0,
    clipboard: '',
    inputValue: '',
    character: ['个人', '商户', '社区'],
    warn: ['确诊', '疑似', '举报'],
    openid: '',
    memberid: '',
    nicheng: '',
    touxiang: '/images/tabbar/about.png',
    IsShanghu: false,
    IsShequ: false,
    IsGeren: true,
    Jiatingdizhi: '',
    JiatingWeizhi: {},
    Shanghudizhi: '',
    ShanghuWeizhi: {},
    Dianhua: '',
    ChangjingOpenids: [],
    ShanghuOpenids: [],
    baogaoweizhi: 0,
    jubao: 0,
    polyline: null,
    markers: null,
    location: null,
    fakeData:[],
    timestamp:'',
    shijian:''

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.loadMap();
    wx.getClipboardData({
      success: function (res) {
        console.log('剪贴板：', res.data);
        console.log(that.checkString(res.data));
        if (that.checkString(res.data)) {
          that.setData({
            clipboard: res.data,
          });

        }
      }

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.onTime();
    this.getMemberInfo();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  PickerChange_c(e) {
    console.log(e);
    this.setData({
      index_c: e.detail.value
    })
  },
  PickerChange_w(e) {
    console.log(e);
    this.setData({
      index_w: e.detail.value
    })
  },
  checkString: function (str) {
    // 判断字符串是否为数字和字母组合

    if (escape(str).indexOf("%u") < 0) {
      return true;
    } else {
      return false;
    }
  },
  getMemberInfo: function () {
    var that = this;
    const db = wx.cloud.database({
      env: 'lbs-dba82caa'
    });
    db.collection('Member').where({
      Openid: this.data.clipboard
    }).get({
      success: res => {
        console.log("会员信息：" + res.data[0].nicheng);
        if (res.data[0]._id !== '') {
          console.log('memberid: ', res.data[0]._id);
          app.globalData.memberid = res.data[0]._id;
          that.setData({
            inputValue: that.data.clipboard,
            memberid: res.data[0]._id,
            IsShanghu: res.data[0].IsShanghu,
            IsShequ: res.data[0].IsShequ,
            IsGeren: res.data[0].IsGeren,
            Jiatingdizhi: res.data[0].Jiatingdizhi,
            JiatingWeizhi: res.data[0].JiatingWeizhi,
            Shanghudizhi: res.data[0].Shanghudizhi,
            ShanghuWeizhi: res.data[0].ShanghuWeizhi,
            Dianhua: res.data[0].Dianhua,
            ChangjingOpenids: res.data[0].ChangjingOpenids,
            ShanghuOpenids: res.data[0].ShanghuOpenids,
            baogaoweizhi: res.data[0].baogaoweizhi,
            jubao: res.data[0].jubao,
            nicheng: res.data[0].nicheng,
            touxiang: "/images/tabbar/about_cur.png",
          });
        }
        if (res.data[0].IsShanghu) {
          that.setData({
            index_c: 1,
            index_c_original: 1
          });
        }
        else if (res.data[0].IsShequ) {
          that.setData({
            index_c: 2,
            index_c_original: 2
          });
        }

      },
      fail: err => {

      }
    });

  },
  characterChange: function () {
    var that = this;
    if (that.data.inputValue){

      // 调用云函数
      wx.cloud.callFunction({
        name: 'memberManage',
        data: {
          "openid": that.data.inputValue,
          "index_c": that.data.index_c,
          "index_c_original": that.data.index_c_original
        },
        success: res => {

          wx.showToast({
            title: '角色修改成功',
            icon: 'success',
            image: '',
            duration: 1000,
            mask: true,
            success: function (res) { },
            fail: function (res) { },
            complete: function (res) { },
          })

        },
        fail: err => {
          console.error('[角色修改云函数]调用失败', err)
        }
      })

    }
    else{
      wx.showToast({
        title: '没有会员数据',
        icon: 'loading',
        image: '',
        duration: 1000,
        mask: true,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    }


  },
  warnSend: function () {
    var that = this;
    if (this.data.ShanghuOpenids.length !== 0) {
      const db = wx.cloud.database();
      db.collection('News').add({
        // data 字段表示需新增的 JSON 数据
        data: {
          dizhi: that.data.Jiatingdizhi,
          Openids: that.data.ShanghuOpenids,
          Tuwen: '社区工作人员发布预警',
          Weizhi: db.Geo.Point(that.data.location.longitude, that.data.location.latitude),
          Yingxiangfanwei: 10000,
          pic: '',
          shijian: '',
          status: that.data.warn[that.data.index_w],
          title: "社区工作人员发布预警:" + that.data.warn[that.data.index_w] + "病例"


        }
      })
        .then(res => {

          // 调用云函数
          wx.cloud.callFunction({
            name: 'sendWarn',
            data: {
              latitude: Number(that.data.latitude),
              longitude: Number(that.data.longitude),
            },
            success: res => {
              wx.showToast({
                title: '发布预警成功',
                icon: 'success',
                image: '',
                duration: 1000,
                mask: true,
                success: function (res) { },
                fail: function (res) { },
                complete: function (res) { },
              })
            },
            fail: err => {
              console.error('[云函数] [getOpenid] 调用失败', err)
            }
          })

          console.log(res)

        })
        .catch(console.error)
    }
    else {
      wx.showToast({
        title: '没有轨迹数据',
        icon: 'loading',
        image: '',
        duration: 1000,
        mask: true,
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    }

  },
  bindRemarkInput(e) {
    this.setData({
      remark: e.detail.value
    })
  },
  mapTap(e) {
  },
  loadMap() {
    MapUtil.getLocation((res) => {
      this.setData({
        location: res.result.location,
        address: res.result.address,
        polyline: [{
          points: [{
            latitude: res.result.location.lat,
            longitude: res.result.location.lng
          }, {
            latitude: res.result.location.lat,
            longitude: res.result.location.lng
          }],
          color: "#ff6600",
          width: 4,
          dottedLine: false,
          arrowLine: true,
          borderColor: "#000",
          borderWidth: 5
        }],
        markers: [{
          iconPath: '/images/marker.png',
          id: 0,
          latitude: res.result.location.lat,
          longitude: res.result.location.lng,
          width: 32,
          height: 32,
        }],
      })
    })
  },
  regionchange(e) {
    // 地图发生变化的时候，获取中间点，也就是用户选择的位置toFixed
    if (e.type == 'end') {
      console.log(e)
      const mapCtx = wx.createMapContext("map");
      mapCtx.getCenterLocation({
        type: 'gcj02',
        success: (res) => {
          console.log(res);
          mapCtx.translateMarker({
            markerId: 0,
            destination: {
              longitude: res.longitude,
              latitude: res.latitude
            },
            duration: 200,
            success: () => {
              const req = res;
              MapUtil.reverseGeocoder(req, (res) => {
                if(this.data.nicheng ==="虚拟用户"){
                  this.setData({
                    Jiatingdizhi: res.result.address,
                  });
                }
                this.setData({
                  address: res.result.address,
                  location: {
                    latitude: req.latitude,
                    longitude: req.longitude
                  }
                });
              });
            }
          })
        },
        failed: (err) => {
          console.log(err)
        }
      })
    }
  },
  markertap(e) {
    console.log(e);
    var that = this;
    const db = wx.cloud.database();
    this.setData({
      fakeData: [that.data.timestamp, db.Geo.Point(that.data.location.longitude, that.data.location.latitude),that.data.openid]
    })
    wx.showModal({
      title: '确认标记此位置吗？',
      content: '系统将把此位置信息标记为疫情相关（确诊、疑似、举报）地理数据',
      showCancel: true,
      cancelText: '取消',
      cancelColor: '',
      confirmText: '确定',
      confirmColor: '',
      success: function (res) {
        
        that.data.ShanghuOpenids.push(that.data.fakeData[0], that.data.fakeData[1],that.data.fakeData[2]);
       
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  onTime: function () {
    //获取当前时间戳;
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    console.log("当前时间戳为：" + timestamp);
    this.setData({
      timestamp: timestamp,
    })
    //获取当前时间
    var n = timestamp * 1000;
    var date = new Date(n);
    //年
    var Y = date.getFullYear();
    //月
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    //日
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    //时
    var h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    //分
    var m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    //秒
    var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

    console.log("当前时间：" + Y + '-' + M + '-' + D + " " + h + ":" + m + ":" + s);




    this.setData({
      shijian: Y + '-' + M + '-' + D + " " + h + ":" + m + ":" + s,
    })
  },
  virtualMember: function(){
    var that = this;
    that.setData({
      inputValue: "virtual" + app.globalData.memberid,
      memberid: app.globalData.memberid,
      IsShanghu: false,
      IsShequ: false,
      IsGeren: true,
      Jiatingdizhi: that.data.address,
      JiatingWeizhi: null,
      Shanghudizhi: that.data.address,
      ShanghuWeizhi: null,
      Dianhua: '',
      ChangjingOpenids: [],
      ShanghuOpenids: [],
      baogaoweizhi: 0,
      jubao: 0,
      nicheng: "虚拟用户",
      touxiang: "/images/tabbar/about_cur.png",
    });
  }
})