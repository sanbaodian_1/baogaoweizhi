// pages/login/login.js
const app = getApp();
//定义一些常量
const messageTmplId = 'krxZ64shtstbWqugqnIbNOcjCSRIXFp11y5bpLthWMU';
var x_PI = 3.14159265358979324 * 3000.0 / 180.0;
var PI = 3.1415926535897932384626;
var a = 6378245.0;
var ee = 0.00669342162296594323;
var QR = require('../../utils/qrcode.js');
const QQMapWX = require('../../lib/js/qqmap-wx-jssdk.min.js');
var qqmapsdk;
var intervalPic = null;
var village_LBS = function (that) {
  // ------------ 腾讯LBS地图  --------------------
  wx.getLocation({
    type: 'gcj02', //返回可以用于wx.openLocation的经纬度
    success: function (res) {
      // 调用接口, 坐标转具体位置 -xxz0717
      qqmapsdk.reverseGeocoder({
        location: {
          latitude: Number(res.latitude),
          longitude: Number(res.longitude)
        },
        success: function (res) {
          console.log("地理信息获取：")
          console.log(res);
          that.setData({
            start_address: res.result.address, //起点地址
            myProvince: res.result.address_component.province,
            myCity: res.result.address_component.city, //起点城市
            myDistrict: res.result.address_component.district, //区
            myLatitude: Number(res.result.location.lat),
            myLongitude: Number(res.result.location.lng),
            myAddress: res.result.formatted_addresses.recommend,
            province: res.result.address_component.province,
            city: res.result.address_component.city,
            district: res.result.address_component.district,
            latitude: Number(res.result.location.lat),
            longitude: Number(res.result.location.lng),
            weizhi: Number(res.result.location.lat) + ',' + Number(res.result.location.lng),
          })
        }
      });

    }
  })
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAR3klEQVR4Xu1de5QlRXn/fX3v7LoMMDsbwHWBLDs7d7qqRzaGhyKPw6LCASUYXRVWEzAIIiICAQ2PJDwFgUh4KEbQ8DgaQQgiCgiCEBAIICiQ6ao7d3cWswQihBDZXdh53P5yintHYeHe7ttd/ZhHnbNn/+iq3/eo33TXrarv+wizbUZ7gGa09bPGY5YAM5wEswSYJcAM98AMN3/2DTBLgBnugRlu/rR9A/j+yGKi0R0B551AsIyZtifCFszcTYRugLoBbGnmnxn/R4QNAG9gNv9jHZEzwownAXqKed7jg4Pb/+905Mq0IECtVtt6fDzYk4h3Z8ZuAL+LiDa3PGEvAPxrAA+af0T0kBBinWUZmcNNWQIopVYy0wFE2ANAX+aeawh8khm/KJXoJtd1785Jh0RipxQBfL+2E9HEXzHjk0S0IJHl9gc/DdBVXV2lq/r7+9fah08HcUoQQGt9NDN/HqB3puMGu6jMuNtxcJkQ4kd2ke2jFZYAQ0NDc4jKRxDxKQC2s296Foj8GIAzpJQ/yUJaHBmFI8D0mPg3TcUvmZ0zPG/g1jiTlOaYQhHA96sfIeKvAViSptE5Yj9QLpc+W6lU/Bx1eIPoQhBAa+0y87cA2rsojklRj4AZVzB3n1aEvYVcCaC13iIIcAERPpeiwwsJzcwvOQ6dKoT4pzwVzI0AWutlzDCr5B3ydEABZN8D8MellC/moUsuBPD96glA8FUimpOH0UWTycy/dRxaIYR4IGvdMiXAmjVr5m/cuPF6gPbL2tApIC8A+BwhxJlEFGSlb2YEaBzOjN07+8oPm1r+SRAEKwYHB8fCetp4ngkBGqt83AdgGxtKzwCMB4Kgvv/g4OD6tG1NnQDVanXXep3vJML8tI2ZZvhPBUH38rR/KqZKAN8f3heo30xEm02zycnEHGaulUrOPq7r/ldaAlMjgFLqvQD9HMDb0lJ+huBWg6B797TeBKkQoFareRMT9X8HsMUMmaRUzWTmX82f37PnokWLXrEtyDoBtNZLmGEmf3bBZ3e2fi6Eux8R1W3CWiXAqlWrthkbG3+EiBbbVHIW6/ceuEFK8Qmb/rBKAN/X9xNhT5sKzmJt6gE+SUppTkytNGsE0FqfxoxzrGiVCgg/BODfAKwB8DSAN2y0OI4zr15HHxEPAFgOYFkqalgAZS7t7HmVxy1A2YkNVErtDlDm+9hhDmDmhwHn247D13d6g1drvYiZDmUODieiSpisjJ+P9PbOX7Zw4UJzhT1RS/wGMPv7r746+iQRtk+kidXBfAURXSSEqNqAVUotZ6bjifBhG3h2MPj7UspPJsVKTACl9HcBfCqpIjbGM+P+Uok+67qutoG3KYbWeh9m/HNRzjOI8HEhxI1JbE1EAKVqewN1c8CTeyPC2UKIv09bkVqttuX4+MTVRPSRtGWF4/NzPT09/Un2B2ITgJnLWlf/A4AbrmiqPV5uXqi4M1Upm4ArVT0Z4K8AcLKUu6ksZlzoeeLLcXWITQDf139DhK/GFWxjHDOPl0rOHq7rPmoDr1MM39dmXfCPnY6z2d/4wHFox7jrnVgEqNVq201M1IcBzLNpTKdYzDjC88R3Oh1ns79S6hqADrWJGQPrXinFPjHGxfsZqJS6GqDD4gi0N4YvklKeaA8vHlLjU6jvA+i98RBsjeI/ixOA0vEbwPdXV4jGzV9/jo0fklLunqMCbxBtopMnJuqr8z384sellDt36pOOCVCEn32lkvOnAwMDJlS7MK0IayKzTyGEuKUTp3REgGq1KoKAVScCbPdl5h96nvyobVwbeL6vniOihTaw4mF0/hboiABF+Ot3HHqP67qPxHNQuqOaPw3PS1dKGHpna4HIBBgaWrvAcTbkErwwabLZ2/c8uVuYC/J63rj2PvpSXvIbcvlWKeWBUXWITACl1BcAuiwqcDr96EQp3YvSwbaD6vv6NiIcYActFkq9XC69o1KpvBBldGQC+L5+mAjvjgKaXh9eIqU0R7mFbb5fNTkNrsxTQSIcL4S4JIoOkQiglNoBIHOOnmd7VkqxbZ4KRJFdhIUywI9JKXeJom9EAlS/AvCpUQBT7GP9OlRauiqlfzeZgi4tGWG4ROgTQoT+0UYigO+rISLywoSm+ZwIfyuEMIcvhW9K6V8Ar2Uvy7HRKVK6oWc1oQRo7nI9n6Mlr4lmppWe516Xtx5R5Pu+vpYIfxmlb3p9+GdSytAg3FAC+H71ECL+fnqKRkXm3aWU5l5f4ZvW+ixm/F2eijLzK54nTTbUti2UAFrrK82pWxhQ2s+DoLTT4GDlV2nLsYGvtT6OGRfbwEqC4Ti0l+u65nPUsoUSQCltDjnyysT5OsV5mZTyqSQOyWqs71cPJeJrspLXZnpPl9I9KzYBhobWLHSc0efyN8RoMHUIoLU+qJn+JlfXMfNdnif3jU0ApdR+AN2RqxW/F857SClNoubCtwKtm56TUi5KQoBjAbq0CB4326tCiJ8WQZcwHZRSRwGUa/avSR2DoD63XbaRtmsApdTXATomzOAsnhPhYCHED7KQlVSG7+svE+H8pDg2xodFEYUR4E6A2n5DbCgZBYOZjvE89/IoffPuo5T6GkB/nbcer62cQvZP2hLA9/V/Fifih78upTy2CE4N00EpdUdxMqHRGVK6Z7bSOeQNoDnM2KyeR1nRZqVLmByltEnp0nbxFYZh8flVUorDOybA2rVr561fv8F6RooEhj0jpShQ/OFbW1KEizObaHaLlKJlTGPLN0ABDUG5XNom6kWHBERLNFRr/WFm3JwIxO7gB6QULXM2tCRAtVrdNgj4Gbu6JEMjwuFCiKuSoaQ7WiltLmJ8MV0p0dGZoTxPtDzJbUmAWq22dGKiviq6qCx68nVSypVZSIorQyn1VJFK25g8xJ4nW95UbkcAk+lrKK4j0hnHL0opt0oHOznqyMjI20dHx/47OZJdBClFy3lu+aAYEUBvdgQzfcjz3NvsusgOmlLqJIAutINmDSWQUpQ6/hXg+/47iJxnralhCYgZ13ueOMQSnFUYpbQJmhFWQZODvSyl6OmYAENDQ5s7TqmIlTE39vbO38pGfpzkvv0DQqOmYd1UCStaa/vzecpsBL3Rq8WLDyhGxPRbfTJj/gowUL6v1qVQg9fGX8gLQrjbEtG4DbCkGKtWrdp+bGx8hIjKSbFsj2fGI54n3tPxJ8AMUEqbfYCC3sXnz0kpv2XbYXHwfF9/s6iFr8K20MNOAx/MP/FByyl5pqdnSzdJgqQ4k73pmOHh4b56PTDX5grZiPBtIcSRMd8AhUh/0saxfJ6UMteAFaW0SYkfKz1LNozhL0kp/yEuAU4FqLDBGM0ESW6UCJg0nK21/hgzbkgD2x4mHySl/HFcAqwAKFEiQnuGtESKnSApiW7NUHCzU1qUY9+3NIe5a8DzltZiEaBZ3PGJJI7KYiwzfcHz3G9kIWtShlL6dgD7Zykzhqy6EO6cdmXo2i4C16xZ87aNG0dfjSE48yFRgyFtKKaUOhGglt9VGzJsYDCz73lysB1WlMAQkwW8MBm5WhvDjwVBsDztUmu+P/wBouBnNiYobQxmXO55ou2l3ggEKERoeCRfmRQyCxb0vj+tbeJm7P/DeYd+R3IGgCjJpEMJUK1W3x8EfFdUoQXod4+U4n1p6FGcMLlo1gVB9x+FVRsLJUCtVps7Pj6xvojbnK3cMHfunIV9fX2/jeamaL2Kkfkjmq7NXk9IKd4VNiKUAAZAKXUvQHuHgRXluYnNF0KYOgbWmlLVIwG+whpg6kDRUulGIoAp907Ehc7O9UZ/8mlSynNt+rhod/3CbGN29va8AVOvuW2LRABTDm58fMLqKzVMsWTP2wdDxMFWSn8PQOISLXFkdzqGGWs9T/xxlHGRCND4DEyJjY/XbCbCJ4QQVrdoixQnGTaxnVRP6YAAaiVA/xImvADPN86Z07Vw6dKlJlOXtaaU+jRAhb6SPmlsuVzqr1QqkU4oIxPARAqtW7f++YJeEHndRPNRUkrri7XVq1f3jI6OmUsfC6yxKh2gB6UUkTOURSaA0dX39eVEODodvZOjMuMEzxOp5eaZCruAnQbPdESAgmQMfQum8J1A+VwpK6YyaKpNKbUjgFMAKlyASieLv0kndUSA5mKwIHUC2VQtudpxnGtd1zXRuJm2xq1p52MAmXyAptRsrtXDjPFxTkU7JkAzYMRU5Ox4rIUZYoBvYy5d7HkDhdme9v2RxcDYMQB/Jsc1wv8I4S7q9KJsrEn0fXVTxoUTNwJ8bVdX14X9/f0Fi1f8A63N8fno6OhKsxYBYD4VGbZ4VcVjEaDxHaQns7GOLguCOecODi4pXMxdO/uVGv4oUD8zo0DRZ3t75w/EOQWNRYDGWqB6KcCppWwxt1mJ6Iw8vu82id0gQnB6muXokyTQik2AZuiYSUduO1r3xnK5dHLUjQybk5UWFjOTUsMHA8HZRNRvWU6i4+/YBGjsC9hLiWouc5RKzrF5lYG1PCkt4Zo5BE361m3syGRXShm7jmMiAjR/FpoKXrsmMCYA2FT+Pqvd5cUE+IUb2ky/Y+ovJTxcSh4XkZgAJpPI+PjEr+NsEZvsFY5DK4QQ5t7hjGta6/2DgE0p+rfHMP6pIKjv0i4LaBTMxARovAXUgQC1DD5oocg95XLp4KInfYrixCR9TEGO8fGJHxNRywDOt8B/mXnOMs/r+00S2WasFQI01gP6AiJ8KYJCZjPnfCHEaTPllR/mE2bu0lpfB1CUiqjsOLSv67p3h+FGeW6NAE0S3EeEvdoJjlPfNooh06FPtBSzfJaU0vystNKsEqB5c8iUdWlRYMJZIeXATVY0n6Yg7U5c06ibbJUAZk6ayRIeffPChj4tpVuAKhrFZ45S6kaAVmyi6YNCuMs73esPs9Y6AYzAoaHaoOPUzZtgi6YCl0opjgtTZvZ5wwPMXNa6+ksAf9L0yRNBUN8zjainVAhglNZa78EMc2L3GyHcHW0zd7qTxZwwEo2ZGkkvBEH3rmEBHnH9kRoBGovC4X2JgsOkFH8RV8GZOq553+BKx3FOSvM8JFUCmMmrVqvvrteDkx2HDhNCFDHtXOE41szTfD7Ax0kpX0xTwdQJYJRvHh9fUi6XDq1UKoVKQJ2mc+NgN3Iy8Om9vb2Hxjne7VRmJgRorgmWMOO7jkOfd1238EknOnWkjf6+X/0gwCs9T2RWdjYzAjQ/B1sFAX8H4FvTuLptYxLywGDmklLVM4loTIiBc7LcIc2UAJPObSRVxk69vb1HZvGay2NSo8o0OZkB+kap5FwUVuY1KmYn/XIhQHNdsDNAFwB8/FQpCduJY6P01VrvEwQ4qrt73tGLFy9+KcoY231yI4AxpBFtM34uEf+up6fnnLyTPtp2biu8arW6VRAEpzM7Ku9SeLkSYNJB1Wp1ryAIzgZwmZTyX7OaiKzlNL71w18kCnZl5hM9z8u9LnMhCPC6tcF5AHYOgvIJg4OVglUrSUaXZh3mk5npMs9zf5gMzd7oQhHAmGXOEYjqJxPxZo7jXOy67v32zM0eSSll4gQOJ3LunzOnfIntqOWkFhWOAJMGaa0XTQZYEOGbruveQkSFKWTZzvGNmovrP8NMHyTim8vl8jWVSmU06WSlMb6wBJg0trknbqpxH8iMR0slutF1XXMRtXDN94c/5DjBnzOjhwjfE0L8qHBKbqJQ4Qnwen19v7YbUD+CCLsAfDtz+QbPqzyep5MbB171TwE0CLAJmbtGCFG4WkutfDSlCDBphHnFbtiw4ZAgwEEmBo+Iza3iO4Jg85+mdWz6uk+Ty8z7MdMBRNgOoHsdBz/IYxPHBvGnJAE2Nbxx94D3A+gDzDBlW6pEtJoZI0TB6rlz567uNG+gKQQxMYGlRPU+Zuoj4n5mLAHwNODc5Th8e15p6m1M/CTGtCDApg4ZHh6WzLxDEAT9jclDHzMWml8WAG3GzAERbWDGOoDLRNgcoG6Ax5npFbMxBdAzRBhh5hHHcVY7jrNmOp5kTksChP2FaK23cBxnawBb1+v1ia6uruf7+/vXho2bjs9nJAGm40TGtWmWAHE9N03GzRJgmkxkXDNmCRDXc9Nk3CwBpslExjVjlgBxPTdNxv0/n2OS28BmSdUAAAAASUVORK5CYII=',
    userInfo: {},
    openid: '',
    memberid: '',
    logged: false,
    nicheng: '',
    touxiang: '',
    showQrcode: false,
    maskHidden: true,
    qrimagePath: '',
    baogaoweizhi: 0,
    jubao: 0,
    mapHeight: 675,
    register: false,
    IsShanghu: false,
    IsShequ: false,
    IsGeren: true,
    Jiatingdizhi: '',
    JiatingWeizhi: {},
    Shanghudizhi: '',
    ShanghuWeizhi: {},
    Dianhua: '',
    ChangjingOpenids: [],
    ShanghuOpenids: [],
    start_address: '',
    weizhi: '',
    province: '',
    city: '',
    district: '',
    longitude: '',
    latitude: '',
    myProvince: '',
    myCity: '',
    myDistrict: '',
    myAddress: '',
    myLatitude: 0.0,
    myLongitude: 0.0,
    text: '显示地图中心所在位置区级疫情数据',
    markers: [],
    markerId: '',
    polyline: [],
    circles: [],
    iconList: [{
      icon: 'warn',
      color: 'red',
      badge: 0,
      name: '周边预警',
      url: '/pages/nearby/nearby'
    }, {
      icon: 'qr_code',
      color: 'orange',
      badge: 0,
      name: '扫码记录',
      url: '/pages/qrhistory/qrhistory'
    },
    {
      icon: 'questionfill',
      color: 'mauve',
      badge: 0,
      name: '系统帮助',
      url: '/pages/help/help'
    }, {
      icon: 'picfill',
      color: 'yellow',
      badge: 0,
      name: '提供线索',
      url: '/pages/report/report'
    }, {
      icon: 'noticefill',
      color: 'olive',
      badge: 0,
      name: '社区通知',
      url: '/pages/notification/notification'
    }, {
      icon: 'search',
      color: 'cyan',
      badge: 0,
      name: '信息检索',
      url: '/pages/search/search'
    }, {
      icon: 'discoverfill',
      color: 'purple',
      badge: 0,
      name: '宣传海报',
      url: '/pages/createPoster/createPoster'
    }, {
      icon: 'brandfill',
      color: 'mauve',
      badge: 0,
      name: '发布通知',
      url: '/pages/newsPost/newsPost'
    }, {
      icon: 'commandfill',
      color: 'purple',
      badge: 0,
      name: '会员管理',
      url: '/pages/memberManage/memberManage'
    }
    ],
    gridCol: 3,
    skin: false,
    ShowYiqing: false,
    Yiqings: [],
    /**
     * 2.6新增地图markers红橙黄,确诊调用 https://ncov.html5.qq.com/api/getCommunity?province=%E5%90%89%E6%9E%97%E7%9C%81&district=%E5%85%A8%E9%83%A8&lat=43.83327&lng=125.28845
     */
    markers: [],
    Hongs: [],
    Chengs: [],
    Huangs: [],
    Quezhens: [],
    scrollHeight:675,
    modalName:null,


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    qqmapsdk = new QQMapWX({
      key: '5DLBZ-LI4KX-FPJ4F-7ZPFC-OVSUZ-FABJK'
    });
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          scrollHeight: res.windowHeight
        });
      }
    });

    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        // console.log(res);
        var latitude = res.latitude
        var longitude = res.longitude
        that.setData({
          latitude: latitude,
          longitude: longitude
        })
      }
    })
    village_LBS(that);
    //转换为时间格式字符串
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              that.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo,
                nicheng: res.userInfo.nickName,
                touxiang: res.userInfo.avatarUrl,
              })



            }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    wx.getSetting({
      success: (res) => {
        console.log(res);
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          console.log('昵称头像授权：' + res.authSetting['scope.userInfo']);
          wx.getUserInfo({
            success: res => {
              that.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo,
              })
              if (app.globalData.openid === '') {
                that.onGetOpenid();
              }
              that.onTime();
              that.getFujin();
              that.getYiqing();
              if (that.data.province && that.data.city && that.data.district) {
                that.getQuezhen();
              }
            }
          })
        }

        console.log('位置授权：' + res.authSetting['scope.userLocation']);
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) { //非初始化进入该页面,且未授权
          wx.showModal({
            title: '请授权当前位置',
            content: '疫情地图需要获取您的地理位置，否则地图功能将无法使用',
            success: function (res) {
              if (res.cancel) {
                console.info("1授权失败返回数据");

              } else if (res.confirm) {
                //village_LBS(that);
                wx.openSetting({
                  success: function (data) {
                    console.log(data);
                    if (data.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 5000
                      })
                      //再次授权，调用getLocationt的API
                      village_LBS(that);
                      if (app.globalData.openid === '') {
                        that.onGetOpenid();
                      }
                      that.onTime();
                      that.getFujin();
                      that.getYiqing();
                      if (that.data.province && that.data.city && that.data.district) {
                        that.getQuezhen();
                      }
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'success',
                        duration: 5000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) { //初始化进入
          village_LBS(that);
          if (app.globalData.openid === '') {
            that.onGetOpenid();
          }
          that.onTime();
          that.getFujin();
          that.getYiqing();
          if (that.data.province && that.data.city && that.data.district) {
            that.getQuezhen();
          }
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;

    this.getYiqing();
    this.getFujin();
    if (this.data.province && this.data.city && this.data.district) {
      this.getQuezhen();
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },



  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //适配不同屏幕大小的canvas
  setCanvasSize: function () {
    var size = {};
    try {
      var res = wx.getSystemInfoSync();
      var scale = 750 / 686; //不同屏幕下canvas的适配比例；设计稿是750宽
      var width = res.windowWidth / scale;
      var height = width; //canvas画布为正方形
      size.w = width;
      size.h = height;
    } catch (e) {
      // Do something when catch error
      console.log("获取设备信息失败" + e);
    }
    return size;
  },
  createQrCode: function (url, canvasId, cavW, cavH) {
    //调用插件中的draw方法，绘制二维码图片
    QR.qrApi.draw(url, canvasId, cavW, cavH);

  },
  //获取临时缓存照片路径，存入data中
  canvasToTempImage: function () {
    var that = this;
    wx.canvasToTempFilePath({
      canvasId: 'mycanvas',
      success: function (res) {
        var tempFilePath = res.tempFilePath;
        console.log("********" + tempFilePath);
        that.setData({
          qrimagePath: tempFilePath,
        });
      },
      fail: function (res) {
        console.log(res);
      }
    });
  },
  //点击图片进行预览，长按保存分享图片
  previewImg: function (e) {
    var img = this.data.qrimagePath;
    console.log(img);
    wx.previewImage({
      current: img, // 当前显示图片的http链接
      urls: [img] // 需要预览的图片http链接列表
    })
  },
  qrcodeSubmit: function (e) {
    var that = this;
    if (app.globalData.openid === '') {
      this.onGetOpenid();
    } else {
      var url = app.globalData.openid;
      if (url) {

        that.setData({
          maskHidden: false,
        });

        wx.showToast({
          title: '加载二维码…',
          icon: 'loading',
          duration: 1000
        });


        var st = setTimeout(function () {
          wx.hideToast()
          var size = that.setCanvasSize();
          //绘制二维码
          that.createQrCode(url, "mycanvas", size.w, size.h);
          that.setData({
            maskHidden: true
          });
          clearTimeout(st);
        }, 2000)
      } else {
        wx.showToast({
          title: '没有用户ID',
          icon: '',
          image: '',
          duration: 0,
          mask: true,
          success: function (res) { },
          fail: function (res) { },
          complete: function (res) { },
        })
      }

    }




  },
  doRegister: function () {
    var that = this;
    if (that.data.touxiang !== '') {
      that.onTime();
      that.onGetOpenid();
    } else {
      wx.showToast({
        title: '请先授权',
        icon: '',
        image: '/images/tabbar/plugin.png',
        duration: 2000,
        mask: true,
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) {
          that.onGetUserInfo();
        },
      })
    }



    // 调用云函数
    wx.cloud.callFunction({
      name: 'register',
      data: {
        nicheng: that.data.userInfo.nickName,
        touxiang: that.data.userInfo.avatarUrl,
        openid: that.data.openid,
        dizhi: that.data.start_address,
      },
      success: res => {
        console.log(res)
        console.log('memberid: ', res.result._id)
        app.globalData.memberid = res.result._id;
        that.setData({
          register: true,
          memberid: res.result._id,
          showQrcode: true,
          maskHidden: false
        })
        that.qrcodeSubmit();
        if (that.data.province && that.data.city && that.data.district) {
          that.getQuezhen();
        }
        that.getYiqing();
        that.getFujin();



        const db = wx.cloud.database();

        db.collection('Logs').add({
          // data 字段表示需新增的 JSON 数据
          data: {
            openid: that.data.openid,
            log:[],

          },
          success: function (res) {
            console.log('注册成功！');
            wx.showToast({
              title: '注册成功！',
            })

          }

        })

      },
      fail: err => {
        console.error('[激活云函数] 调用失败', err)
      }
    })


  },
  onGetUserInfo: function (e) {

    if (!this.logged && e.detail.userInfo) {
      var that = this;
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        touxiang: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo,

      })
      if (app.globalData.openid === '') {
        that.onGetOpenid();
      }


    }


  },
  onGetOpenid: function () {
    var that = this;
    // 调用云函数
    wx.cloud.callFunction({
      name: 'getOpenid',
      data: {},
      success: res => {

        app.globalData.openid = res.result.event.userInfo.openId;
        that.setData({
          openid: res.result.event.userInfo.openId,
        })

      },
      fail: err => {
        console.error('[云函数] [getOpenid] 调用失败', err)
      }
    })

    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {
        nicheng: that.data.userInfo.nickName,
        touxiang: that.data.userInfo.avatarUrl,
        openid: that.data.openid,
      },
      success: res => {
        console.log('注册用户确认' + that.data.openid);
        console.log(res.result);


        if (res.result.data.length !== 0) {
          console.log(res.result.data.length);
          console.log('memberid: ', res.result.data[0]._id);
          app.globalData.memberid = res.result.data[0]._id;
          that.setData({
            register: true,
            memberid: res.result.data[0]._id,
            IsShanghu: res.result.data[0].IsShanghu,
            IsShequ: res.result.data[0].IsShequ,
            IsGeren: res.result.data[0].IsGeren,
            Jiatingdizhi: res.result.data[0].Jiatingdizhi,
            JiatingWeizhi: res.result.data[0].JiatingWeizhi,
            Shanghudizhi: res.result.data[0].Shanghudizhi,
            ShanghuWeizhi: res.result.data[0].ShanghuWeizhi,
            Dianhua: res.result.data[0].Dianhua,
            ChangjingOpenids: res.result.data[0].ChangjingOpenids,
            ShanghuOpenids: res.result.data[0].ShanghuOpenids,
            baogaoweizhi: res.result.data[0].baogaoweizhi,
            jubao: res.result.data[0].jubao,

          })

        
        if (res.result.data[0].IsShanghu === true) {
          that.setData({
            gridCol: 6,
            showQrcode: true,
            maskHidden: false
          })
          that.qrcodeSubmit();
        } else if (res.result.data[0].IsShequ === true) {
          that.setData({
            gridCol: 9,
            showQrcode: true,
            maskHidden: false
          })
          that.qrcodeSubmit();
        } else if (res.result.data[0].IsGeren === true) {
          that.setData({
            gridCol: 3,
            showQrcode: true,
            maskHidden: false
          })
          that.qrcodeSubmit();
        }
        }
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
      }
    })





  },
  onTime: function () {
    //获取当前时间戳;
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    console.log("当前时间戳为：" + timestamp);
    this.setData({
      timestamp: timestamp,
    })
    //获取当前时间
    var n = timestamp * 1000;
    var date = new Date(n);
    //年
    var Y = date.getFullYear();
    //月
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    //日
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    //时
    var h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    //分
    var m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    //秒
    var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

    console.log("当前时间：" + Y + '-' + M + '-' + D + " " + h + ":" + m + ":" + s);




    this.setData({
      shijian: Y + '-' + M + '-' + D + " " + h + ":" + m + ":" + s,
    })
  },
  bindCodeScanTap: function () {
    wx.scanCode({
      scanType: ['qrCode'],
      success: (res) => {
        console.log(res.result);
        if (res.result) {
          this.setData({
            scanValue: res.result,
          });

          const db = wx.cloud.database({
            env: 'lbs-dba82caa'
          });
          const _ = db.command;
          var that = this;
          that.onTime();
          console.log(that.data.timestamp, new db.Geo.Point(that.data.myLongitude, that.data.myLatitude), res.result);
          var toOpenid = res.result;
          db.collection('Member').doc(that.data.memberid).update({
            // data 字段表示需新增的 JSON 数据
            data: {
              ShanghuOpenids: _.push(that.data.timestamp),

            },
            success: function (res) {
              db.collection('Member').doc(that.data.memberid).update({
                // data 字段表示需新增的 JSON 数据
                data: {
                  ShanghuOpenids: _.push(new db.Geo.Point(that.data.myLongitude, that.data.myLatitude)),
                },
                success: function (res) {
                  db.collection('Member').doc(that.data.memberid).update({
                    // data 字段表示需新增的 JSON 数据
                    data: {
                      ShanghuOpenids: _.push(toOpenid),
                      baogaoweizhi: _.inc(1),
                    },
                    success: function (res) {


                      db.collection('Logs').where({
                        openid:that.data.openid,
                      })
                      .update({
                        data: {
                          log: _.push(that.data.shijian),
                         
                        },
                        success: function (res) {
                          db.collection('Logs').where({
                            openid: that.data.openid,
                          })
                            .update({
                              data: {
                                log: _.push(that.data.start_address),

                              },
                              success: function (res) {
                                db.collection('Logs').where({
                                  openid: that.data.openid,
                                })
                                  .update({
                                    data: {
                                      log: _.push(toOpenid),

                                    },
                                    success: function (res) {
                                      console.log('打卡成功！');
                                      wx.showToast({
                                        title: '打卡成功！',
                                      })
                                      that.setData({
                                        baogaoweizhi:that.data.baogaoweizhi + 1
                                      })

                                    }

                                  })

                              }

                            })

                        }

                      })


                    }

                  })

                }

              })

            }

          })



        }
      }
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  gridchange: function (e) {
    this.setData({
      gridCol: e.detail.value
    });
  },
  gridswitch: function (e) {
    this.setData({
      gridBorder: e.detail.value
    });
  },
  canvasToImage() {
    var that = this;
    //  canvas画布转成图片
    wx.canvasToTempFilePath({
      quality: 1,
      fileType: 'jpg',
      canvasId: 'mycanvas',
      success: function (res) {
        wx.hideLoading();
        console.log('成功生成：' + res.tempFilePath);
        that.setData({
          img_temp: res.tempFilePath
        })
        // wx.previewImage({
        //   current: res.tempFilePath, // 当前显示图片的http链接
        //   urls: [res.tempFilePath] // 需要预览的图片http链接列表
        // })
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res) {
            console.log(res);
            wx.showModal({
              title: '',
              content: '图片已保存到相册!',
              showCancel: false,
              confirmText: '好的',
              confirmColor: '#72B9C3',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定');
                }
                that.setData({
                  visible: false
                })
              }
            })
          },
          fail: function (res) {
            console.log(res.errMsg);
            if (res.errMsg === "saveImageToPhotosAlbum:fail auth deny") {
              wx.openSetting({
                success(settingdata) {
                  console.log(settingdata)
                  if (settingdata.authSetting['scope.writePhotosAlbum']) {
                    that.canvasToImage(); //保存失败尝试再次保存
                  } else {
                    console.log('获取权限失败，给出不给权限就无法正常使用的提示')
                  }
                }
              })
            }
          }
        })

      },
      fail: function (res) {
        console.log(res)
      }
    }, this)
  },
  isYiqing(e) {

    //刷新按钮
    var that = this;
    this.setData({
      ShowYiqing: !e.detail.value
    });
    if (!this.data.ShowYiqing) {
      this.setData({
        Yiqings: [],
        markers: [],
      });

      //this.getPoiList(that.data.longitude, that.data.latitude);
      if (this.data.province && this.data.city && this.data.district) {
        this.getQuezhen();
      }
      this.getYiqing();
      this.getFujin();


    }


    /**
     * 2.6同时刷新地图MARKERS
     */
  },
  getYiqing: function () {

    var that = this;
    // 调用云函数
    wx.cloud.callFunction({
      name: 'getYiqing',
      data: {
        longitude: that.data.longitude,
        latitude: that.data.latitude,
      },
      success: res => {
        console.log('云函数：附近疫情')
        console.log(res.result)

        if (res.result && res.result.data.length !== 0) {
          console.log('查到疫情');
          for (var i = 0; i < res.result.data.length; i++) {
            var data = res.result.data[i];
            that.data.Yiqings.push({
              id: data._id,
              latitude: data.Weizhi.coordinates[1],
              longitude: data.Weizhi.coordinates[0],
              pic: data.pic,
              width: 30,
              height: 30,
              title: data.title,
              status: data.status,
              Shijian: data.Shijian,
              Yingxiangfanwei: data.Yingxiangfanwei,
              juli: that.getDistance(that.data.myLatitude, that.data.myLongitude, data.Weizhi.coordinates[1], data.Weizhi.coordinates[0]),

            });
          }
        }
        that.setData({
          Yiqings: that.data.Yiqings,
        })



      },
      fail: err => {
        console.error('[疫情] 调用失败', err)
      }
    })
  },
  Icontap(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  /**
   * 2.6附近预警信息
   */
  getFujin: function () {

    var that = this;

    if (this.data.ShowYiqing === true) {
      this.setData({
        ShowYiqing: false,
        markers: [],
      })
    }

    // 调用云函数
    wx.cloud.callFunction({
      name: 'getFujin',
      data: {
        longitude: that.data.longitude,
        latitude: that.data.latitude,
      },
      success: res => {
        console.log('云函数：周边预警')
        console.log(res.result)
        if (res.result && res.result.data.length !== 0) {
          for (var i = 0; i < res.result.data.length; i++) {
            console.log('查到预警');
            var data = res.result.data[i];

            if (data.status == "疑似") {
              if (that.getDistance(that.data.latitude, that.data.longitude, data.Weizhi.coordinates[1], data.Weizhi.coordinates[0]) < data.Yingxiangfanwei) {
                that.data.markers.push({
                  id: data._id,
                  latitude: data.Weizhi.coordinates[1],
                  longitude: data.Weizhi.coordinates[0],
                  iconPath: '/images/cheng.png',
                  width: 20,
                  height: 32,
                  title: data.title
                });
              }
              if (data.Openids.length !== 0) {

                for (var t = 0; t < data.Openids.length / 3; t++) {

                  that.data.markers.push({
                    id: data._id + t,
                    latitude: data.Openids[t * 3 + 1].coordinates[1],
                    longitude: data.Openids[t * 3 + 1].coordinates[0],
                    iconPath: '/images/huang.png',
                    width: 20,
                    height: 32,
                    title: "疑似病例生活轨迹曾经由此处！"
                  });
                }
              }
            } else if (data.status == "确诊") {
              if (that.getDistance(that.data.latitude, that.data.longitude, data.Weizhi.coordinates[1], data.Weizhi.coordinates[0]) < data.Yingxiangfanwei) {
                that.data.markers.push({
                  id: data._id,
                  latitude: data.Weizhi.coordinates[1],
                  longitude: data.Weizhi.coordinates[0],
                  iconPath: '/images/hong.png',
                  width: 20,
                  height: 32,
                  title: data.title
                });
              }
              if (data.Openids.length !== 0) {

                for (var t = 0; t < data.Openids.length / 3; t++) {

                  that.data.markers.push({
                    id: data._id + t,
                    latitude: data.Openids[t * 3 + 1].coordinates[1],
                    longitude: data.Openids[t * 3 + 1].coordinates[0],
                    iconPath: '/images/cheng.png',
                    width: 20,
                    height: 32,
                    title: "确诊病例生活轨迹曾经由此处！"
                  });
                }
              }
            } else if (data.status == "举报") {
              if (that.getDistance(that.data.latitude, that.data.lLongitude, data.Weizhi.coordinates[1], data.Weizhi.coordinates[0]) < data.Yingxiangfanwei) {
                that.data.markers.push({
                  id: data._id,
                  latitude: data.Weizhi.coordinates[1],
                  longitude: data.Weizhi.coordinates[0],
                  iconPath: '/images/huang.png',
                  width: 20,
                  height: 32,
                  title: data.title
                });
              }
            }


          }
        }
        that.setData({
          markers: that.data.markers,
        })


      },
      fail: err => {
        console.error('[预警] 调用失败', err)
      }
    })

  },
  //计算两坐标点之间的距离

  getDistance: function (lat1, lng1, lat2, lng2) {

    lat1 = lat1 || 0;

    lng1 = lng1 || 0;

    lat2 = lat2 || 0;

    lng2 = lng2 || 0;

    var rad1 = lat1 * Math.PI / 180.0;

    var rad2 = lat2 * Math.PI / 180.0;

    var a = rad1 - rad2;

    var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;

    var r = 6378137;

    return (r * 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(rad1) * Math.cos(rad2) * Math.pow(Math.sin(b / 2), 2)))).toFixed(0)

  },
  getQuezhen: function () {
    var that = this;
    // 调用云函数
    wx.cloud.callFunction({
      name: 'getQuezhen',
      data: {
        province: that.data.province,
        city: that.data.city,
        district: that.data.district,
        longitude: that.data.longitude,
        latitude: that.data.latitude,
      },
      success: res => {
        var province = that.data.province;
        var city = that.data.city;
        var district = that.data.district;
        console.log('云函数：确诊病例小区地址');
        console.log(res.result);
        if (res.result && res.result.community[province] && res.result.community[province][city] && res.result.community[province][city][district]) {
          console.log("查到确诊");
          var quezhen = res.result.community[province][city][district];

          for (var t = 0; t < quezhen.length; t++) {
            that.data.markers.push({
              id: "quezhen" + t,
              latitude: quezhen[t].lat,
              longitude: quezhen[t].lng,
              iconPath: '/images/hong.png',
              width: 20,
              height: 32,
              title: quezhen[t].full_address + "发现确诊病例！"
            });
          }

          that.setData({
            markers: that.data.markers,
          })
        }



      },
      fail: err => {
        console.error('[确诊] 调用失败', err)
      }
    })
  },
  CopyLink(e) {
    wx.setClipboardData({
      data: e.currentTarget.dataset.link,
      success: res => {
        wx.showToast({
          title: '已复制会员ID',
          duration: 1000,
        })
      }
    })
  },
  onShareAppMessage() {
    return {
      title: '疫情地图，小心疫疫！',
      path: '/pages/login/login'
    }
  },
  isFull(e) {
    console.log("点击放大地图")

    if (this.data.mapHeight === 675) {
      this.setData({
        mapHeight: 1000,
      });
    } else {
      this.setData({
        mapHeight: 675,
      });
    }


    /**
     * 2.7地图放大
     */
  },
  getMymarkers: function () {
    wx.showToast({
      title: '已经标注轨迹',
      duration: 1000,
    })

    const db = wx.cloud.database({
      env: 'lbs-dba82caa'
    });
    var that = this;
    db.collection('Member').where({
      _id: that.data.memberid,
    })
      .get({
        success: function (res) {
          // res.data 是包含以上定义的两条记录的数组
          console.log("我的生活轨迹");
          console.log(res.data[0].ShanghuOpenids);
          var guiji = res.data[0].ShanghuOpenids;
          for (var t = 0; t < guiji.length / 3; t++) {
            that.data.markers.push({
              id: "guiji" + t,
              latitude: guiji[t * 3 + 1].latitude,
              longitude: guiji[t * 3 + 1].longitude,
              iconPath: '/images/ren.png',
              width: 20,
              height: 32,
              title: "我曾到过此处"
            });
          }
          that.setData({
            markers: that.data.markers,
          })
        }
      })




  },
  /**
   * 获取中间点的经纬度，并mark出来
   */
  getLngLat() {
    let page = this;
    page.mapCtx = wx.createMapContext("map");
    page.mapCtx.getCenterLocation({
      success: function (res) {
        page.setData({

          longitude: res.longitude,
          latitude: res.latitude,

        })
        page.getPoiList(res.longitude, res.latitude);
      }
    })

  },
  getPoiList(longitude, latitude) {
    let page = this
    qqmapsdk.reverseGeocoder({
      location: {
        latitude: latitude,
        longitude: longitude,
      },
      duration: 200,
      success: function (res) {
        console.log("地址切换")
        console.log(res);
        /**
         * 详细数据从这儿拿....
         */
        page.setData({
          text: "地图中心位置：" + res.result.address,
          province: res.result.address_component.province,
          city: res.result.address_component.city, //起点城市
          district: res.result.address_component.district, //区

        });


      },
      fail: function (res) {
        console.log(res);

      },
      complete: function (res) {
        if (page.data.province && page.data.city && page.data.district) {
          page.getQuezhen();          
        }

      }
    });
  },

  markertap(e) {

    console.log(e);
  },

  /**
   * 回到自己位置
   */
  moveToLocation: function () {
    let mpCtx = wx.createMapContext("map");
    mpCtx.moveToLocation();

    if (this.data.province && this.data.city && this.data.district) {
      this.getQuezhen();
    }

  },

  regionchange(e) {



    // 地图发生变化的时候，获取中间点，也就是用户选择的位置toFixed

    if (e.type == 'end' && e.causedBy == 'drag') {

      console.log("拖动")

      var that = this;

      let mapCtx = wx.createMapContext("map");

      mapCtx.getCenterLocation({

        type: 'gcj02',

        success: function (res) {

          that.setData({

            latitude: res.latitude,

            longitude: res.longitude,

            circles: [{

              latitude: res.latitude,

              longitude: res.longitude,

              color: '#FF0000DD',

              fillColor: '#d1edff88',

              radius: 1, //定位点半径

              strokeWidth: 1

            }]

          })
          that.getLngLat();
        }


      })

    }

  },

  //定位到自己的位置事件

  my_location: function (e) {

    var that = this;

    that.onLoad();

  },

  gcj02towgs84(lng, lat) {

    var that = this;

    if (that.out_of_china(lng, lat)) {

      return [lng, lat]

    } else {

      var dlat = that.transformlat(lng - 105.0, lat - 35.0);

      var dlng = that.transformlng(lng - 105.0, lat - 35.0);

      var radlat = lat / 180.0 * PI;

      var magic = Math.sin(radlat);

      magic = 1 - ee * magic * magic;

      var sqrtmagic = Math.sqrt(magic);

      dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * PI);

      dlng = (dlng * 180.0) / (a / sqrtmagic * Math.cos(radlat) * PI);

      var mglat = lat + dlat;

      var mglng = lng + dlng;

      return [lng * 2 - mglng, lat * 2 - mglat]

    }

  },

  transformlat(lng, lat) {

    var ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng));

    ret += (20.0 * Math.sin(6.0 * lng * PI) + 20.0 * Math.sin(2.0 * lng * PI)) * 2.0 / 3.0;

    ret += (20.0 * Math.sin(lat * PI) + 40.0 * Math.sin(lat / 3.0 * PI)) * 2.0 / 3.0;

    ret += (160.0 * Math.sin(lat / 12.0 * PI) + 320 * Math.sin(lat * PI / 30.0)) * 2.0 / 3.0;

    return ret

  },

  transformlng(lng, lat) {

    var ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng));

    ret += (20.0 * Math.sin(6.0 * lng * PI) + 20.0 * Math.sin(2.0 * lng * PI)) * 2.0 / 3.0;

    ret += (20.0 * Math.sin(lng * PI) + 40.0 * Math.sin(lng / 3.0 * PI)) * 2.0 / 3.0;

    ret += (150.0 * Math.sin(lng / 12.0 * PI) + 300.0 * Math.sin(lng / 30.0 * PI)) * 2.0 / 3.0;

    return ret

  },


  /**

  * 判断是否在国内，不在国内则不做偏移

  * @param lng

  * @param lat

  * @returns {boolean}

  */

  out_of_china(lng, lat) {

    return (lng < 72.004 || lng > 137.8347) || ((lat < 0.8293 || lat > 55.8271) || false);

  },
  onPullDownRefresh() {
    var that = this;
    this.setData({
      markers: [],
      Yiqings: [],
    })
    if (this.data.markers === []) {
      wx.showToast({
        title: '数据更新....',
        icon: 'loading'
      })
    }


    this.getYiqing();
    this.getFujin();
    if (this.data.province && this.data.city && this.data.district) {
      this.getQuezhen();
    }
  },
  isQRcode(e) {

    //刷新按钮
    var that = this;
    this.setData({
      showQrcode: !that.data.showQrcode,
      maskHidden: !that.data.maskHidden
    });

    that.qrcodeSubmit();


  },
  onSubscribe: function (e) {
    var that = this;
    const item = [this.data.myLongitude, this.data.myLatitude];
    if(item ===[0,0]){
      const db = wx.cloud.database({
        env: 'lbs-dba82caa'
      });
      const _ = db.command;
      // 调用微信 API 申请发送订阅消息
      wx.requestSubscribeMessage({
        // 传入订阅消息的模板id，模板 id 可在小程序管理后台申请
        tmplIds: [messageTmplId],
        success(res) {
          // 申请订阅成功
          if (res.errMsg === 'requestSubscribeMessage:ok') {
            // 这里将订阅的课程信息调用云函数存入db
            wx.cloud
              .callFunction({
                name: 'subscribe',
                data: {
                  ...item,
                  data: {
                    thing4: {
                      value: that.data.myAddress
                    },
                    thing3: {
                      value: that.data.myDistrict
                    },
                    thing5: {
                      value: "今日周边疫情数据已更新！"
                    },
                    weizhi: new db.Geo.Point(that.data.myLongitude, that.data.myLatitude),
                    shijian: that.data.timestamp,
                  },
                  templateId: messageTmplId,
                },
              })
              .then(() => {
                wx.showToast({
                  title: '订阅成功',
                  icon: 'success',
                  duration: 2000,
                });
              })
              .catch(() => {
                wx.showToast({
                  title: '订阅失败',
                  icon: 'loading',
                  duration: 2000,
                });
              });
          }
        },
      });
    }
    else{
      wx.showToast({
        title: '没有位置数据',
        icon: 'loading',
        duration: 2000,
      });
      village_LBS(that);
    }
   
  },



})