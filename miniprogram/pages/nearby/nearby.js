const db = wx.cloud.database();
const table = db.collection("Report");

// pages/zhoubianyujing/zhoubianyujing.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reportList: []
  },
  getReportList() {
    var that = this;

    const _ = db.command
    db.collection('Report').where({
      location: _.geoNear({
        geometry: new db.Geo.Point(125.28845,43.83326),
        minDistance: 0,
        maxDistance: 50000,
      })
    })
      .orderBy('date', 'desc')
      .get()
      .then((res) => {
        console.log(res);
        that.setData({
          reportList: res.data
        })
      })







  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getReportList();
  },

  gotoDetail(e) {
    const id = e.currentTarget.id
    wx.navigateTo({ url: `/pages/detail/detail?id=${id}` })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})