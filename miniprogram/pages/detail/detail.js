const db = wx.cloud.database();
const table = db.collection("Report");

// pages/zhoubianyujing/zhoubianyujing.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    report: {}
  },
  getReportDetail(id) {
    var _this = this;
    //因为skip的参数不能为0,所以只能这样判断
    table.where({
        _id: id
      }).get()
      .then((res) => {
        console.log(res);
        _this.setData({
          report: res.data[0]
        })
        console.log(this.data.report.imgList[0])
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getReportDetail(options.id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})