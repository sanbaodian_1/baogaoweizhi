const db = wx.cloud.database();
const table = db.collection("Report");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    CustomBar: app.globalData.CustomBar,
    keyword: "",
    reportList: []
  },
  gotoDetail(e){
    const id = e.currentTarget.id
    wx.navigateTo({ url: `/pages/detail/detail?id=${id}` }) 
  },
  getReportList() {
    const _ = db.command;
    const keyword = this.data.keyword;
    table.orderBy('date', 'desc')
      .where(_.or([
        {
          title: db.RegExp({
            regexp: keyword,
            option: 'i'
          })
        },
        {
          remark: db.RegExp({
            regexp: keyword,
            option: 'i'
          })
        },
        {
          location: db.RegExp({
            regexp: keyword,
            option: 'i'
          })
        }
      ]))
      .get()
      .then((res) => {
        this.setData({
          reportList: res.data
        })
      })
  },
  searchKey(e) {
    this.setData({
      keyword: e.detail.value
    })
    this.getReportList();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getReportList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})