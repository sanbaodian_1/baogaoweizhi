const cloud = require('wx-server-sdk')
cloud.init()
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  const db = cloud.database();
  var openid ='';
  var character = false;
  if (wxContext.OPENID ==='o_R4p49CJ2_apBb4yCWVgXTEwwNw' && event.openid !== '' && event.index_c !== '' && event.index_c_original !== '') {
    openid = event.openid;
    index_c_original = event.index_c_original;
    index_c = event.index_c;
  }
  try {
    return await db.collection('Member').where({
      Openid: openid
    })
      .update({
        data: {
          IsGeren: event.index_c - 0 === 0,
          IsShanghu: event.index_c - 1 === 0,
          IsShequ: event.index_c - 2 === 0,
        },
      })
  } catch (e) {
    console.error(e)
  }

}