const cloud = require('wx-server-sdk')
const axios = require('axios')
var rp = require('request-promise');
cloud.init({
  env: 'lbs-dba82caa'
})

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event)
  try {
    const resultValue = await rp('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxe2182ea812f81f19&secret=d0482c95ab156ddb0a5bc5c6a8e0cda7')
    const token = JSON.parse(resultValue).access_token;
    console.log('------ TOKEN:', token);

    const response = await axios({
      method: 'post',
      url: 'https://api.weixin.qq.com/wxa/getwxacodeunlimit',
      responseType: 'stream',
      params: {
        access_token: token,
      },
      data: {
        page: event.page,
        width: 300,
        scene: event.id,
      },
    });

    return await cloud.uploadFile({
      cloudPath: 'QRcodes/' + Date.now() + '.png',
      fileContent: response.data,
    });
  } catch (err) {
    console.log('>>>>>> ERROR:', err)
  }
}