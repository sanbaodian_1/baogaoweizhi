// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {

  try {
    return await db.collection('messages').where({
      weizhi: _.geoNear({
        geometry: new db.Geo.Point(Number(event.longitude), Number(event.latitude)),
        minDistance: 0,
        maxDistance: 10000,
      })
    })
      .update({
        data: {
          done: false,
        },
      })
  } catch (e) {
    console.error(e)
  }

}