const cloud = require('wx-server-sdk')
//引入request-promise用于做网络请求
var rp = require('request-promise');
cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  let url = `https://ncov.html5.qq.com/api/getCommunity?province=${event.province}&city=${event.city}&district=${event.district}&lat=${event.latitude}&lng=${event.longitude}`;
  return await rp(url)
    .then(function (res) {
      return res
    })
    .catch(function (err) {
      return '失败'
    });
}