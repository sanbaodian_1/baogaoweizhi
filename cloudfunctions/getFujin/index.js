// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {

    try {
      return await db.collection('News').where({
        Weizhi: _.geoNear({
          geometry: new db.Geo.Point(event.longitude, event.latitude),
          minDistance: 0,
          maxDistance: 50000,
        })
      })
        .get()
    } catch (e) {
      console.error(e)
    }

  }



