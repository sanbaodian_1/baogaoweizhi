const cloud = require('wx-server-sdk')
//引入request-promise用于做网络请求
var rp = require('request-promise');
cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  let url = `https://ncov.html5.qq.com/api/getCommunity?province=${encodeURI(event.province)}&city=${encodeURI(event.city)}&district=${encodeURI(event.district)}&lat=${event.latitude}&lng=${event.longitude}`;
  return await rp(url)
    .then(function (res) {
      console.log(res)
      return JSON.parse(res)
    })
    .catch(function (err) {
      console.log(err)
      return '失败'
    });
}