// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
const db = cloud.database()
const _ = db.command
exports.main = async (event, context) => {
  if (event.nicheng && event.touxiang && event.openid) {
    try {
      return await db.collection('Member').add({
        // data 字段表示需新增的 JSON 数据
        data: {
          Openid: event.openid,
          _openid: event.openid,
          nicheng:event.nicheng,
          IsShanghu: false,
          IsShequ: false,
          IsGeren: true,
          Jiatingdizhi: event.dizhi,
          JiatingWeizhi: {},
          Shanghudizhi: event.dizhi,
          ShanghuWeizhi: {},
          Dianhua: '',
          ChangjingOpenids: [],
          ShanghuOpenids: [],
          baogaoweizhi:0,
          jubao:0,
        }
      })
    } catch (e) {
      console.error(e)
    }
  } 
}